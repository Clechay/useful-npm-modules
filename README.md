# List of most useful npm modules.



## CLI

#### args parsing

* [commander](https://github.com/tj/commander.js "github repo") - process.argv parser

#### in program console support

* [inquirer](https://github.com/sboudrias/Inquirer.js "github repo") - async CLI input module
* [quick-cli](https://github.com/Clechay/quick-cli "github repo") - simple sync console I/O tool based on inquirer and chalk
* [chalk](https://github.com/chalk/chalk "github repo") - terminal string styling


## flow control

#### just async

* [async](https://github.com/caolan/async "github repo") - tool for controling  asynchronous flow

#### promises based

* [q](https://github.com/kriskowal/q "github repo") - tool for creating and composing asynchronous promises
* [bluebird](https://github.com/petkaantonov/bluebird "github repo") - fully featured promise library with focus on innovative features and performance

#### async function in sync code

* [deasync](https://github.com/abbr/deasync "github-repo") - turns async function into sync via JavaScript wrapper of Node event loop

## utils

#### cross-platform

* [osenv](https://github.com/npm/osenv "github-repo") - look up environment settings specific to different operating systems

#### object clone

* [clone](https://github.com/pvorb/node-clone "github-repo") - deep cloning of objects and arrays

#### object merge

* [merge](https://github.com/yeikos/js.merge "github-repo") - merge multiple objects into one, optionally creating a new cloned object

## time

* [moment](https://github.com/moment/moment "github repo") - parse, validate, manipulate, and display dates

## files and media

#### file system

* [fs-extra](https://github.com/jprichardson/node-fs-extra "github repo") - adds file system methods that aren't included in the native fs module, it is a drop in replacement for fs

#### pdf support

* [pdf-text-extract](https://github.com/nisaacson/pdf-text-extract "github repo") - simple async tool for extracting text from pdf document


## text preprocess

#### css

* [less](https://github.com/less/less.js "github-repo") - JavaScript, official, stable version of Less

#### template engines

* [jade](https://github.com/jadejs/jade "github repo") - whitespace sensitive syntax HTML template engine
* [ejs](https://github.com/mde/ejs "github-repo") - embedded JavaScript templates

#### minifier/compressor

* [clean-css](https://github.com/jakubpawlowicz/clean-css "github-repo") - fast and efficient Node.js library for minifying CSS


## web severs

#### express.js

* [express](https://github.com/strongloop/express "github repo") - simple and powerful web framework for node
* [basic-auth](https://github.com/jshttp/basic-auth "github repo") - generic basic auth Authorization header field parser for whatever middleware
* [body-parser](https://github.com/expressjs/body-parser "github repo") - http request body parser middleware
* [cookie-parser](https://github.com/expressjs/cookie-parser "github repo") - cookie parsing with signatures middleware
* [morgan](https://github.com/expressjs/morgan "github repo") - HTTP request logger middleware
* [multer](https://github.com/expressjs/multer "github repo") - multipart/form-data handling middleware; primarily used for uploading files
* [serve-favicon](https://github.com/expressjs/serve-favicon "github repo") - favicon serving middleware with caching

## HTTP requests

* [request](https://github.com/request/request "github repo") - simplified HTTP request client

## development workflow management

#### gulp
* [gulp](https://github.com/gulpjs/gulp "github-repo") - streaming build system
* [gulp-nodemon ](https://github.com/JacksonGariety/gulp-nodemon "github-repo") - nodemon for use with gulp tasks

## process managers

#### development
* [nodemon](https://github.com/remy/nodemon "github repo") - simple monitor script for use during development of a node.js app

#### production

* [pm2](https://github.com/Unitech/pm2 "github repo") - production process manager for Node.JS applications with a built-in load balancer
* 
* [forever](https://github.com/nodejitsu/forever "github repo") - simple CLI tool for ensuring that a given node script runs continuously

## shell

* [shelljs](https://github.com/arturadib/shelljs "github repo") - a portable (Windows/Linux/OS X) implementation of Unix shell commands on top of the Node.js API
